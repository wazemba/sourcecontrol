/// @DnDAction : YoYo Games.Drawing.Set_Font
/// @DnDVersion : 1
/// @DnDHash : 137F3701
/// @DnDArgument : "font" "fnt_game"
/// @DnDSaveInfo : "font" "fnt_game"
draw_set_font(fnt_game);

/// @DnDAction : YoYo Games.Common.Set_Global
/// @DnDVersion : 1
/// @DnDHash : 12064DC1
/// @DnDInput : 2
/// @DnDArgument : "value_1" "3"
/// @DnDArgument : "var" "player_score"
/// @DnDArgument : "var_1" "player_lives"
global.player_score = 0;
global.player_lives = 3;