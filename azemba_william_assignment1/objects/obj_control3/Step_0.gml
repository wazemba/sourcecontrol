/// @DnDAction : YoYo Games.Common.If_Variable
/// @DnDVersion : 1
/// @DnDHash : 43376E06
/// @DnDArgument : "var" "player_lives"
/// @DnDArgument : "op" "3"
if(player_lives <= 0)
{
	/// @DnDAction : YoYo Games.Rooms.Go_To_Room
	/// @DnDVersion : 1
	/// @DnDHash : 68E7AFA6
	/// @DnDParent : 43376E06
	/// @DnDArgument : "room" "rm_end"
	/// @DnDSaveInfo : "room" "rm_end"
	room_goto(rm_end);
}